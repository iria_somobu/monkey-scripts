// ==UserScript==
// @name     Joyreactor warinig remover
// @namespace https://gitlab.com/iillyyaa2033/monkey-scripts
// @description Removes non-blockable 'you are using ablocker!' banner on joyreactor.cc
// @include *joyreactor.cc/* 
// @include *.reactor.cc/*
// @grant   none
// @version 1
// @author  Iria.Somobu
// ==/UserScript==

const targetNode = document.getElementById('pageinner');
const config = { childList: true };

const callback = function(mutationsList, observer) {
  for(let mutation of mutationsList) {
    if (mutation.type === 'childList') {

      for(let node of mutation.addedNodes){
        if(node.id == "content"){
          node.remove();
        }
      }
          
    }
  }
};

const observer = new MutationObserver(callback);
observer.observe(targetNode, config);