// ==UserScript==
// @name        	Discord local usernotes
// @namespace   	https://gitlab.com/iillyyaa2033/monkey-scripts
// @description 	...
// @match         https://discord.com/* 
// @grant   			none
// @version 			1
// @author  			Iria.Somobu
// ==/UserScript==


let TYPE_NOTE = 'note';


/**
 * Step 1: capture DOM mutation
 */
const callback = function(mutationsList, observer) {
  for(let mutation of mutationsList) {
    
    if (mutation.type === 'childList') {
    	for(let node of mutation.addedNodes){
        if((""+node.id).startsWith("popout"))	detect_user_popout(node)
      }

    }
  }
};


/**
 * Step 2: detect wether is is user popout
 */
function detect_user_popout(node) {
	var container = node.querySelector("div > div").querySelector("div > div")
  var is_user_popout = false;
  
  for (let classname of container.className.split(' ')) {
    if (classname.startsWith("userPopout")){
      is_user_popout = true;
      break;
    }
  }
  
  if(is_user_popout) {
    mutate_user_popout(container)
  }
}


/**
 * Step 3: modify user popout
 */
function mutate_user_popout(root) {
	let uid = lookup_user_id(root);
  let uname = lookup_user_name(root);
  console.log("User popout ("+uname+" "+uid+")");
  
  var target_body = null;
  for (let i = 0; i < root.children.length; i++) {
    if((root.children[i].className+"").includes("body-")) {
      target_body = root.children[i];
      break;
    }
  }
  
  if(target_body != null) {
    target_body.appendChild(get_extended_info_view(uid, uname));
  }
}


function lookup_user_id(root) {
	return root.querySelector("img").getAttribute("src").split("/")[4];
}


function lookup_user_name(root) {
	return root.getAttribute("aria-label");
}


function get_extended_info_view(uid, uname) {
  var div = document.createElement('div');
  div.style = "padding-top: 4px; padding-right: 4px; color: white; font-size: 14px;";
  div.innerHTML = "<hr/>" + 
    "<textarea style=\"resize: none; width: 100%; height: 64px; " +
  		"font-size: 12px; background: #222; color: white; border: 1px solid black;\">"+
    "</textarea>";
  
  let ta = div.querySelector("textarea");
  ta.setAttribute("uid", uid);
  ta.value = get_user_info(uid, TYPE_NOTE, "");
  ta.onchange = function(e) { 
    store_user_info(e.target.getAttribute("uid"), TYPE_NOTE, e.target.value) 
  };
  
  return div;
}


function get_user_info(uid, type, fallback) {
  let rz = localStorage.getItem(type+uid)
	return !!rz ? rz : fallback;
}

function store_user_info(uid, type, data) {
	localStorage.setItem(type+uid, data);
}



/**
 * Fire our mutation observer
 */
const targetNode = document.getElementById('app-mount');
const config = { subtree: true, childList: true };

const observer = new MutationObserver(callback);
observer.observe(targetNode, config);


