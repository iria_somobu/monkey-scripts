// ==UserScript==
// @name Direct Links in Google Search
// @namespace https://gitlab.com/iillyyaa2033/monkey-scripts
// @description Remove indirections from Google search results on all TLDs.
// @include https://www.google.tld/*output=search*
// @include http://www.google.tld/*output=search*
// @include https://www.google.tld/search*
// @include http://www.google.tld/search*
// @include https://www.google.tld/#*
// @include http://www.google.tld/#*
// @include https://www.google.tld/webhp*
// @include http://www.google.tld/webhp*
// @include https://www.google.tld/
// @include http://www.google.tld/
// @include https://encrypted.google.com/*
// @version 3.0
// @grant   none
// ==/UserScript==

// Authors:
// - Original namespace: https://github.com/astanin
// - Uploaded by jetxee from userscripts.org, 2012
// - Modified by Iria.Somobu, 2020
//
// Version 3.0
// - rewrite system using MutationObserverAPI
// - fix regex (google has changed url again)
//
// Version 2.2
// - rewrite using DOMNodeInserted and getElements* to support Chromium too
// - enable on encrypted.google.com
// - try to fix new-style classless links in search results (/url?q=...)
//
// Version 2.1
// - enable on plain Google search page, now it works with Instant too
//
// Version 2.0 (Direct Links in Google Search)
// - fork "Less Google", which is not working any more, and rename
// - enable on all Google top-level domains (only .com and .ca in Less Google)
// - enable on new HTTPS search pages (HTTP still supported, just in case)
// - remove deprecated @exclude patterns (search URLs have been changed)
// - don't allow any "onmousedown" link modifications
//
// Version 1.1 (Less Google by @clump)
// - change include to only apply to search results
//   (maps and images are too slow)
//
// Version 1.0 (Less Google by @clump)

const targetNode = document.getElementById('search');
const config = { attributes: true, childList: true, subtree: true };
const callback = function(mutationsList, observer) {
  for(let mutation of mutationsList) {
    if(mutation.type==="attributes" && mutation.attributeName==="href") {
      var href = mutation.target.getAttribute("href");
      var m = href.match(/\/url?.*url=([^&]*)/)
      if (m) {
        mutation.target.setAttribute('href', decodeURIComponent(m[1]));
      }
    }
  }
};

const observer = new MutationObserver(callback);
observer.observe(targetNode, config);